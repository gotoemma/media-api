<?php

namespace Gotoemma\MediaApiBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraints\File as ParentFile;

/**
 * @Annotation
 */
class File extends ParentFile {}
